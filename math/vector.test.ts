import { Vector } from './vector.ts';
import test from '../tester.ts';

var vec1 = new Vector(3, 4);
var vec2 = new Vector(10, 0);
var out: any;

test(
	'vector > mag',
	() => Vector.magnitude(vec1),
	out => out === 5
)

test(
	'vector > mag always abs',
	() => ({
		first: new Vector(-3, 4),
		second: new Vector(-3, -4)
	}),
	(out) => Vector.magnitude(out.second) >= 0 && Vector.magnitude(out.second) >= 0
)

test(
	'vector > normal',
	() => Vector.normal(vec2),
	out => out.x === 1 && out.y === 0
)

test(
	'vector > angle',
	() => Vector.angle(vec2),
	out => out === 90
)

test(
	'vector > dist',
	() => Vector.distance(vec1, vec2),
	out => out === 8.06225774829855
)

test(
	'vector > add',
	() => Vector.add(vec1, vec2),
	out => out.x === 13 && out.y === 4
)

test(
	'vector > sub',
	() => Vector.sub(vec1, vec2),
	out => out.x === -7 && out.y === 4
)

test(
	'vector > limit',
	() => Vector.limit(new Vector(-3, 4), 1),
	(out) => Vector.magnitude(out) == 1
)