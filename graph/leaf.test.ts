import test from '../tester.ts';
import Node from './node.ts';
import Leaf from './leaf.ts';

let root = new Node()
let leaf = new Leaf();

root.add(leaf);

test(
	'leaf > parent is root',
	() => ({ root, leaf }),
	({ root, leaf }) => leaf.parentNode === root
)

test(
	'leaf > disallow children',
	() => leaf,
	(leaf) => {
		try {
			(<any>leaf).add(new Node());
			return false;
		}
		catch {
			return true;
		}
	}
)