import test from '../tester.ts';
import Node from './node.ts';

const root = new Node();
const child = new Node();
const grandchild = new Node();

root.add(child);


test(
	'node > grandchildren',
	() => {
		child.add(grandchild);
	},
	() => grandchild.parentNode == child
)

test(
	'node > no add to self',
	() => { },
	() => {
		try {
			grandchild.add(grandchild);
			return false;
		}
		catch { return true }
	}
)

test(
	'node > move children',
	() => {
		root.add(grandchild);
	},
	() => grandchild.parentNode !== child
)

test(
	'node > no circular ref',
	() => {
		child.add(grandchild);
	},
	() => {
		try {
			grandchild.add(child);
			return false;
		}
		catch { return true }
	}
)

test(
	'node > root is graph root',
	() => { },
	() => grandchild.root == root
)