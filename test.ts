import './graph/leaf.test.ts';
import './graph/node.test.ts';

import './math/vector.test.ts';

import test from './tester.ts';
test.complete();