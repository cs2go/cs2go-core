import * as c from 'https://deno.land/std/fmt/colors.ts';

const UNSET = Symbol('unset');
let ANY_ERROR = false;

const test = (name: string, before: () => any, compare: (out: any) => boolean | void) => {
	let out: any = UNSET;
	let result: boolean | void = false;

	try {
		out = before();
	}
	catch (e) {
		out = false;
		console.error(c.bgRed('Failed to run `before`'));
		console.error(e);
		ANY_ERROR = true;
	}

	try {
		result = compare(out);
	}
	catch (e) {
		console.error(c.bgRed('Failed to run `compare`'));
		console.error(e);
		ANY_ERROR = true;
	}

	const color = result ? 'green' : 'red';
	const printfn = result ? 'log' : 'error';
	const message = result ? 'has passed!' : 'has failed!';

	console[printfn](c.blue(name) + ':', c[color](message));

	if (!result) ANY_ERROR = true;
}


test.complete = () => {
	if (ANY_ERROR) { }
}

export default test;