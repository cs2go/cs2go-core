# CS2GO Core

This library contains some common util stuff.

## Import paths

importing for deno:

```js
import Node from 'https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/graph/node.ts';
import Leaf from 'https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/graph/leaf.ts';

import { rad2deg, deg2rad } from 'https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/angles.ts';
import { IVector, IScalar, Vector } from 'https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/vector.ts';
```


## THIS IS A DENO PROJECT

To include in your nodeJS project, you can build using `deno bundle` command. 

```sh
deno bundle https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/src/bundler.ts $(output)
```

Where `$(output)` is the file you want to save to.